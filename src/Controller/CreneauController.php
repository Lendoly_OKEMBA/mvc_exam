<?php
namespace App\Controller;
use App\Model\CreneauModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class CreneauController extends AbstractController{
    public function allCreneau(){
        $creneaux = CreneauModel::all();
        $this->render('app.rdv.add-creneau', array(
            'creneaux' => $creneaux,
        ));
    }

    public function add(){
        $errors = [];
        if (!empty($_POST['submitted'])){
            //faille XSS
            $post = $this->cleanXss($_POST);
            //validation
            $validation = new Validation();
            $errors['nom'] = $validation->textValid($post['nom'], 'nom', 3, 12);
            $errors['email'] = $validation->emailValid($post['email']);
            if ($validation->isValid($errors)){
                CreneauModel::insert($post);
                $this->redirect('acceuil');
            }
        }
        $form = new Form($errors);
        $this->render('app.rdv.add-creneau', [
            'form' => $form,
        ]);
    }
}