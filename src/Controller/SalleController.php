<?php
namespace App\Controller;
use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class SalleController extends AbstractController{
    public function allSalle(){
        $salles = SalleModel::all();
        $this->render('app.rdv.acceuil', array(
            'salles' => $salles,
        ));
    }

    public function single($id) {
        $categorys = $this->getCategoryByIdOr404($id);
        $this->render('app.category.single',array(
            'categorys' => $categorys,
        ), 'admin');
    }

    public function add(){
        $salles = SalleModel::all();
        $errors = [];
        if (!empty($_POST['submitted'])){
            //faille XSS
            $post = $this->cleanXss($_POST);
            //validation
            $validation = new Validation();
            $errors['nom'] = $validation->textValid($post['title'], 'titre', 3, 12);
            $errors['maxuser'] = $validation->textValid($post['maxuser'],'nombre max d\'utilisateur', 3, 12);
            if ($errors['maxuser'] > 5){
                return 'Vous avez dépassez le nombre d\'utilisatuer !';
            }
            if ($validation->isValid($errors)){
                SalleModel::insert($post);
                $this->redirect('acceuil');
            }
        }
        $form = new Form($errors);
        $this->render('app.rdv.add-salle', [
            'form' => $form,
            'salles' => $salles,
        ]);
    }

    private function getCategoryByIdOr404($id)
    {
        $cat = SalleModel::findById($id);
        if(empty($cat)) {
            $this->Abort404();
        }
        return $cat;
    }
}