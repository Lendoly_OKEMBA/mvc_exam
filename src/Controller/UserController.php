<?php
namespace App\Controller;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class UserController extends AbstractController{
        public function allUser(){
            $users = UserModel::all();
            $this->render('app.rdv.acceuil', array(
                'users' => $users,
            ));
        }

        public function add(){
            $errors = [];
            if (!empty($_POST['submitted'])){
                //faille XSS
                $post = $this->cleanXss($_POST);
                //validation
                $validation = new Validation();
                $errors['nom'] = $validation->textValid($post['nom'], 'nom', 3, 20);
                $errors['email'] = $validation->emailValid($post['email']);
                if ($validation->isValid($errors)){
                    UserModel::insert($post);
                    $this->redirect('acceuil');
                }
            }
            $form = new Form($errors);
            $this->render('app.rdv.add-user', [
                'form' => $form,
            ]);
        }
}