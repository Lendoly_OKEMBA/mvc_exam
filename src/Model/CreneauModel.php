<?php
namespace App\Model;
use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel{
    protected static $table = 'creneau';


    public static function getTable()
    {
        return self::$table;
    }

    private $id;

    public function getId()
    {
        return $this->id;
    }

    private $id_salle;

    public function getIdSalle()
    {
        return $this->id_salle;
    }

    private $start_at;

    public function getStartAt()
    {
        return $this->start_at;
    }

    private $nbrehours;

    public function getNbrehours()
    {
        return $this->nbrehours;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (salle, start_at, nbrehours) VALUES (?,NOW(),?)",
            array($post['salle'], $post['nbrehours'])
        );
    }
}