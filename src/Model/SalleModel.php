<?php
namespace App\Model;
use Core\App;
use Core\Kernel\AbstractModel;

class SalleModel extends AbstractModel{
    protected static $table = 'salle';


    public static function getTable()
    {
        return self::$table;
    }

    private $id;

    public function getId()
    {
        return $this->id;
    }

    private $title;

    public function getSalle()
    {
        return $this->title;
    }

    private $maxuser;

    public function getMaxuser()
    {
        return $this->maxuser;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (title, maxuser) VALUES (?,?)",
            array($post['title'], $post['maxuser'])
        );
    }
}