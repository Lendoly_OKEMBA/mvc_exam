<?php
namespace App\Model;
use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel{
    protected static $table = 'user';


    public static function getTable()
    {
        return self::$table;
    }

    private $id;

    public function getId()
    {
        return $this->id;
    }

    private $nom;

    public function getNom()
    {
        return $this->nom;
    }

    private $email;

    public function getEmail()
    {
        return $this->email;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (nom, email) VALUES (?,?)",
            array($post['nom'], $post['email'])
        );
    }

}