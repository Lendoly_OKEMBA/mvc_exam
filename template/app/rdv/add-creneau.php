<form action="" method="post" novalidate style="display: flex; align-items: center; flex-direction: column " class="wrap">
    <?php echo $form->label('salle');?>
    <?php echo $form->input('salle', 'text');?>
    <?php echo $form->error('salle');?>

    <?php echo $form->label('start_at');?>
    <?php echo $form->input('start_at', 'text');?>
    <?php echo $form->error('start_at');?>

    <?php echo $form->submit('submitted')?>
</form>