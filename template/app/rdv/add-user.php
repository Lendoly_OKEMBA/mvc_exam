<form action="" method="post" novalidate style="display: flex; align-items: center; flex-direction: column " class="wrap">
    <?php echo $form->label('nom');?>
    <?php echo $form->input('nom', 'text');?>
    <?php echo $form->error('nom');?>

    <?php echo $form->label('email');?>
    <?php echo $form->input('email', 'email');?>
    <?php echo $form->error('email');?>

    <?php echo $form->submit('submitted')?>
</form>